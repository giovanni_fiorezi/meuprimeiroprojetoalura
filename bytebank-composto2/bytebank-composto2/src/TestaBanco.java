public class TestaBanco {
	
	public static void main(String[] args) {
		Cliente giovanni = new Cliente();
		giovanni.nome = "Giovanni Fiorezi";
		giovanni.cpf = "525.757.181-00";
		giovanni.profissao = "desenvolvedor";
		
		Conta contaDoGiovanni = new Conta();
		contaDoGiovanni.deposita(100);
		
		// associa o cliente giovanni a conta contaDoGiovanni
		contaDoGiovanni.titular = giovanni;
		System.out.println(contaDoGiovanni.titular.nome);
	}

}
