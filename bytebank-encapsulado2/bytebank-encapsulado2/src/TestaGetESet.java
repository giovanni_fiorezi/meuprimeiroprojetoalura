
public class TestaGetESet {
	
	public static void main(String[] args) {
		Conta conta = new Conta(1337, 24226);
	
		System.out.println(conta.getNumero());
		
		Cliente giovanni = new Cliente();
		//conta.titular = giovanni;
		giovanni.setNome("giovanni fiorezi");
		
		conta.setTitular(giovanni);
		
		System.out.println(conta.getTitular().getNome());
		
		conta.getTitular().setProfissao("desenvolvedor");
		
	}

}
