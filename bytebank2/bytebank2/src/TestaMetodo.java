
public class TestaMetodo {
	
	public static void main(String[] args) {
		Conta contaDoGiovanni = new Conta();
		contaDoGiovanni.saldo = 100;
		contaDoGiovanni.deposita(50);
		System.out.println(contaDoGiovanni.saldo);
		
		boolean conseguiuRetirar = contaDoGiovanni.saca(20);
		System.out.println(contaDoGiovanni.saldo);
		System.out.println(conseguiuRetirar);
		
		Conta contaDaMarcela = new Conta();
		contaDaMarcela.deposita(1000);
		
		if(contaDaMarcela.transfere(3000, contaDoGiovanni)) {
			System.out.println("transferência feita com sucesso!");
		} else {
			System.out.println("faltou dinheiro");
		}
		
		System.out.println(contaDaMarcela.saldo);
		System.out.println(contaDoGiovanni.saldo);
		
		contaDoGiovanni.titular = "Giovanni Fiorezi";
		System.out.println(contaDoGiovanni.titular);
	}

}
